/* (c) 2008 marco (@macarony.de - Matthias Diener) - MIT LICENSE.html */


#define _REMOVE_THIS_LINE_BEFORE_BUILD_


/* cpt will listen on the port defined with PORT_LISTEN
 * 443 makes sense, because most proxy support 443 connects
 * Webserver should be moved to another port
 */
#define PORT_LISTEN 443

/* Define BIND_THIS_IP_ONLY to bind the daemon to listen only
 * on this IP. May be IP v4/v6
 */
/*#define BIND_THIS_IP_ONLY "::ffff:127.0.0.1"*/
/* HOST/PORT_FORWARD are the default route to the (ssl-)websever */
#define HOST_FORWARD "127.0.0.1"
#define PORT_FORWARD 8443
/*
 * for usage with stunnel map from 81 to 80 as default forward
#define PORT_LISTEN 81
#define PORT_FORWARD 80
*/
/* It's recommended to start cpt as root. It will drop privileges
 * to the RUNAS_USER after binding required port
 */
#define RUNAS_USER "nobody"
/* define facility and log_level
 * facility: syslog, log_file or both
 * log_level: CPT_WARN | CPT_INFO | CPT_DEBUG
 * ERROR Messages are always logged, CPT_DEBUG is not advised
 * - different log_level for syslog and log_file are possible
 */
#define USE_SYSLOG CPT_WARN
/*#define USE_LOGFILE CPT_INFO
#define LOG_FILE "/var/log/cpt.log"
*/
/* define warn_level options, level WARN is used instead of INFO:
 * options are: CPT_W_REQUEST | CPT_W_TARGET | CPT_W_HEADER
 * CPT_W_CLIENT_IP: client ip is not listed
 * CPT_W_REQUEST: CONNECT request malformed
 * CPT_W_TARGET: target ist not allowed
 * CPT_W_HEADER: required header are not matched
 */
#define WARN_LEVEL CPT_W_REQUEST | CPT_W_TARGET | CPT_W_HEADER

/* cpt does proxy if the following conditions are successfully passed:
 * check for correct client_ip, target, header.
 */
/* Add a client ckeck to allow only a list of clients
 * uncomment "#define USE_CHECK_CLIENT_IP", to enable client checks
 * The check is successful if ANY client (partly) matches
 * The client check could be like this:
 * static const char* client_required[] = {"127.0.0.1", "192.168.100."};
 */
/* #define USE_CHECK_CLIENT_IP */
#ifdef USE_CHECK_CLIENT_IP
static const char* client_required[] = {""};
#endif /* USE_CHECK_CLIENT_IP */
/* A target can be define in two different ways:
 * HOST_PORT_COMBINATION: one of this CONNECT strings must be matched
 * otherwise: destination port is checked, on match host is tested.
 * both methods have their pros and cons - choose the one that fits
 * It is not reccomended to uncomment the next line, because no target
 * rules will be checked. This allows free connects to everywhere and will
 * open a security leak, that could be used by anyone
 */
/* #define ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK */
/*#define USE_HOST_PORT_COMBINATION */
#ifdef USE_HOST_PORT_COMBINATION
static const char *host_port_allow[] = {"localhost:22", "127.0.0.1:22"};
#else /* USE_HOST_PORT_COMBINATION */
static const char *host_allow[] = {"localhost", "127.0.0.1"};
static const int port_allow[] = {22, 25};
#endif /* USE_HOST_PORT_COMBINATION */

/* Another posibility to raise security is to add required headers
 * Each header in this array must be supplied by the client
 * The client must have ALL headers included to match
 * comment "#define USE_HEADER_REQUIRED" out, to disable header checks
 * If you like to use basic authentication, you could use a header like this:
 * Authorization: Basic dXNlcjpwYXNz
 * This will give access to "user:pass@host", but there will be no reply
 * for authorization.. if none is given the request the CONNECT will get
 * rejected and forwarded to default target
 * It could be also used a non standard header like this:
 * static const char* header_required[] = {"Connect-Password: important"};
 */
#define USE_HEADER_REQUIRED
#ifdef USE_HEADER_REQUIRED
static const char* header_required[] = {"Accept: text/html"};
#endif /* USE_HEADER_REQUIRED */

