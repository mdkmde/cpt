#!/bin/sh
# /etc/init.d/cpt
# 20120118 marco (@macarony.de - Matthias Diener)

# chkconfig: 2345 20 80
# description: cpt - connect proxy tunnel

### BEGIN INIT INFO
# Provides:          cpt
# Required-Start:    $network
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: cpt - connect proxy tunnel
# Description:       cpt - connect proxy tunnel
### END INIT INFO

# This is an example of a start/stop script for SysV-style init, such
# as is used on Linux systems.  You should edit some of the variables
# and maybe the 'echo' commands.
#
# Place this file into /etc/init.d/cpt (or
# /etc/rc.d/init.d/cpt) and make symlinks to
#   /etc/rc.d/rc0.d/K20cpt
#   /etc/rc.d/rc1.d/K20cpt
#   /etc/rc.d/rc6.d/K20cpt
#   /etc/rc.d/rc2.d/S20cpt
#   /etc/rc.d/rc3.d/S20cpt
#   /etc/rc.d/rc4.d/S20cpt
#   /etc/rc.d/rc5.d/S20cpt
# or, If you have chkconfig, simply run:
# chkconfig --add cpt
#
# On debian:
# update-rc.d cpt defaults
#

PREFIX=/usr/local
# set STUNNEL_CONF, if you want to use it
STUNNEL_CONF=${PREFIX}/etc/stunnel_cpt.conf

CPT_BIN="${PREFIX}/sbin/cpt"
PID_FILE=/var/run/cpt/cpt.pid

# The path that is to be used by the script
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin

# Only start if we can find the cpt.
test -x "${CPT_BIN}" || {
  echo "${CPT_BIN} not installed"
  if [ "$1" = "stop" ]; then
    exit 0
  else
    exit 5
  fi
}

STUNNEL_STAT="-"
getStunnelStat() {
  if [ -r "${STUNNEL_CONF}" ]; then
    STUNNEL_STAT="$(which stunnel)"
    STUNNEL_BIN="$(which stunnel)"
    if [ -x "${STUNNEL_BIN}" ]; then
      STUNNEL_PID_FILE="$(awk 'BEGIN { FS="[ =]+"; pid = ""; }; $1 == "chroot" { pid = $2 pid }; $1 == "pid" { pid = pid $2 }; END { printf("%s", pid); };' ${STUNNEL_CONF})"
      STUNNEL_STAT="configured"
      if [ -r "${STUNNEL_PID_FILE}" ]; then
        STUNNEL_STAT="PID_FILE=${STUNNEL_PID_FILE} exists, but not running"
        STUNNEL_PID="$(STUNNEL_PPID=$(cat "${STUNNEL_PID_FILE}" 2>/dev/null) export STUNNEL_PPID; echo "${STUNNEL_PPID}"; ps -opid --no-heading --ppid ${STUNNEL_PPID} 2>/dev/null)"
        if [ -n "$(ps -e | grep "${STUNNEL_PID}")" ]; then
          STUNNEL_STAT="running"
        fi
      fi
    fi
  fi
}

start() {
  if [ -f "${PID_FILE}" ]; then
    if [ -n "$(ps -e | grep "$(cat "${PID_FILE}")")" ]; then
      echo "is running"
    else
      rm -f "${PID_FILE}" 2> /dev/null
      if [ $? -eq 0 ]; then
        $0 start
      else
        echo "ERR - cannot remove PID_FILE=${PID_FILE}"
        exit 1
      fi
    fi
  else
    ${CPT_BIN}
    sleep 1
    if [ $? -ne 0 -o ! -f "${PID_FILE}" ]; then
      echo "ERR - cannot start cpt"
      exit 1
    fi
    getStunnelStat
    if [ "configured" == "${STUNNEL_STAT}" ]; then
      ${STUNNEL_BIN} ${STUNNEL_CONF}
      sleep 1
      STUNNEL_PID="$(STUNNEL_PPID=$(cat "${STUNNEL_PID_FILE}" 2>/dev/null) export STUNNEL_PPID; echo "${STUNNEL_PPID}"; ps -opid --no-heading --ppid ${STUNNEL_PPID} 2>/dev/null)"
      if [ -n "$(ps -e | grep "${STUNNEL_PID}")" ]; then
        STUNNEL_STAT="started"
      fi
    fi
  fi
}

stop() {
  if [ -f "${PID_FILE}" ]; then
    getStunnelStat
    if [ "running" == "${STUNNEL_STAT}" ]; then
      kill ${STUNNEL_PID}
      sleep 1
      kill -9 ${STUNNEL_PID} 2>/dev/null
      STUNNEL_STAT="stopped"
    fi
    kill $(cat "${PID_FILE}") 2> /dev/null
    if [ $? -ne 0 ]; then
      echo "ERR - cannot stop cpt"
      exit 1
    fi
  else
    echo "not running"
  fi
}

# Parse command line parameters.
case $1 in
  start)
  echo -n "Starting cpt: "
  start
  echo "OK (STUNNEL ${STUNNEL_STAT})"
  ;;
  stop)
  echo -n "Stopping cpt: "
  stop
  echo "OK (STUNNEL ${STUNNEL_STAT})"
  ;;
  restart)
  echo -n "Restarting cpt: "
  stop
  sleep 1
  start
  echo "OK (STUNNEL ${STUNNEL_STAT})"
  ;;
  reload|force-reload)
  echo "Nothing to reload - try restart"
  ;;
  status)
  getStunnelStat
  if [ -f "${PID_FILE}" ]; then
    if [ -n "$(ps -e | grep "$(cat "${PID_FILE}")")" ]; then
      echo "OK - running (STUNNEL ${STUNNEL_STAT})"
    else
      echo "ERR - corrupt PID_FILE=${PID_FILE} exists, but not running (STUNNEL ${STUNNEL_STAT})"
      exit 1
    fi
  else
    echo "OK - not running (STUNNEL ${STUNNEL_STAT})"
  fi
  ;;
  *)
  echo "Usage: $0 {start|stop|restart|status}" 1>&2
  exit 1
  ;;
esac

exit 0

