/* (c) 2008 marco (@macarony.de - Matthias Diener) - MIT LICENSE.html */

#define PID_FILE "/tmp/cpt_test.pid" /* ONLY redefined for test purpose */
#define PORT_LISTEN 1023 /* using privileged port for testing */
#define HOST_FORWARD "127.0.0.1"
#define PORT_FORWARD 8443
#define USE_SYSLOG CPT_INFO
#define USE_LOGFILE CPT_DEBUG | CPT_WARN
#define RUNAS_USER "nobody"
#define WARN_LEVEL CPT_W_HEADER

/*#define USE_HOST_PORT_COMBINATION */
#ifdef USE_HOST_PORT_COMBINATION
static const char *host_port_allow[] = {"localhost:80", "127.0.0.1:80"};
#else /* USE_HOST_PORT_COMBINATION */
static const char *host_allow[] = {"localhost", "127.0.0.1"};
static const int port_allow[] = {80};
#endif /* USE_HOST_PORT_COMBINATION */

#define USE_HEADER_REQUIRED
static const char* header_required[] = {"Accept: text/html", "Accept: text/css"};

