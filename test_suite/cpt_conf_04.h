/* (c) 2008 marco (@macarony.de - Matthias Diener) - MIT LICENSE.html */

#define PID_FILE "/tmp/cpt_test.pid" /* ONLY redefined for test purpose */
#define PORT_LISTEN 1023 /* using privileged port for testing */
#define HOST_FORWARD "127.0.0.1"
#define PORT_FORWARD 8443
#define ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK
#define USE_LOGFILE
#define RUNAS_USER "nobody"

