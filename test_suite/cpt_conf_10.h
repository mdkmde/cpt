/* (c) 2008 marco (@macarony.de - Matthias Diener) - MIT LICENSE.html */

#define PID_FILE "/tmp/cpt_test.pid" /* ONLY redefined for test purpose */
#define PORT_LISTEN 1023 /* using privileged port for testing */
#define HOST_FORWARD "127.0.0.1"
#define PORT_FORWARD 8443
#define USE_SYSLOG CPT_INFO
#define USE_LOGFILE CPT_DEBUG | CPT_WARN
#define RUNAS_USER "nobody"
#define WARN_LEVEL CPT_W_HEADER

#define ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK
#define USE_CHECK_CLIENT_IP
static const char* client_required[] = {"::ffff:127."};

#define USE_HEADER_REQUIRED
static const char* header_required[] = {"Accept: text/html", "Accept: text/css"};

