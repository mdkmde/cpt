/* (c) 2008,2009 marco (@macarony.de - Matthias Diener) - MIT LICENSE.html */
#define CPT_ERR 0
#define CPT_WARN 1
#define CPT_INFO 2
#define CPT_DEBUG 4
#define CPT_DEBUG_INFO 6
#define CPT_W_REQUEST 1
#define CPT_W_TARGET 2
#define CPT_W_HEADER 4
#include "cpt_conf.h"
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#ifdef USE_SYSLOG
# if ! (USE_SYSLOG + CPT_ERR)
#  define USE_SYSLOG CPT_ERR
# endif /* ! (USE_SYSLOG + CPT_ERR) */
# include <syslog.h>
#endif /* USE_SYSLOG */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/tcp.h>
#ifdef RUNAS_USER
# include <pwd.h>
#endif /* RUNAS_USER */
#define sizeof_array(array) (sizeof(array) / sizeof(array[0]))

/* if DAEMON_DIR doesn't exist, it will be created */
#ifndef DAEMON_DIR
# define DAEMON_DIR "/var/run/cpt"
#endif /* DAEMON_DIR */
#ifndef PID_FILE
# define PID_FILE DAEMON_DIR"/cpt.pid"
#endif /* PID_FILE */
#define MAX_LISTEN_NUM_CONNECT 20
#define REQUEST_SIZE 255
#define PROXY_REPLY "HTTP/1.0 200 Connection established\r\n\r\n"
#define CONNECTION_TIMEOUT 120
#define RWBUF_SIZE 1024

static struct addrinfo *def_dst_addr;
static int daemon_pid, pid_fd;
#ifdef USE_LOGFILE
# ifndef LOG_FILE
#  define LOG_FILE "/var/log/cpt.log"
# endif /* ! LOG_FILE */
# if ! (USE_LOGFILE + CPT_ERR)
#  define USE_LOGFILE CPT_ERR
# endif /* ! (USE_LOGFILE + CPT_ERR) */
/* use stdout as long as no file is opened */
static int log_fd = STDOUT_FILENO;
#endif /* USE_LOGFILE */

/* do & handle connection for requested target (source fd -> destination addr) */
void do_forward(int src_fd, const struct addrinfo *dst_addr, const char *first_line, int size_of_first_line);
/* check request and apply rules to choose destination target */
void do_request(int req_fd, const char *client_ip);
/* parse request line from fd till (\r)\n */
int get_request_line(int req_fd, char *request_line, int max_request_line_size);
/* print message, do cleanup and exit with given exit_status */
void mprint_clean_exit(int exit_status, const char *msg, ...);
/* get struct addrinfo for given (destination) host & port */
void get_addrinfo(const char *dhost, int dport, struct addrinfo **daddr);
void signal_handler(int sig);
/* return port number and strip port from host_port to hostname */
int split_host_port(char *host_port);
#if (defined(USE_SYSLOG) || defined(USE_LOGFILE))
# if (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) != CPT_INFO)
#  undef CPT_DEBUG
#  define CPT_DEBUG CPT_DEBUG_INFO
# endif /* (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) != CPT_INFO) */
/* if (any) logging is enabled create log_writer (with/without) va_list */
void vwrite_log(int cpt_log_level, const char *msg, va_list ap);
void write_log(int cpt_log_level, const char *msg, ...);
#endif /* (defined(USE_SYSLOG) || defined(USE_LOGFILE)) */

int
main(int argc, char** argv) {
#define MAX_PID_LEN 6
  char pid_s[(MAX_PID_LEN + 1)], client_ip[(NI_MAXHOST + 1)];
  struct flock pid_l;
  struct addrinfo *result, *rp;
  int pid, l_fd, in_fd, sock_opt_flag;
  socklen_t addr_size;
  struct sockaddr_storage in_addr;
#ifdef RUNAS_USER
  struct passwd *pw;
#endif /* RUNAS_USER */

  if((argc == 2) && (strcmp("-v", argv[1]) == 0)) {
    printf("cpt-"VERSION", (c)2008 marco (@macarony.de - Matthias Diener)\n");
    exit(EXIT_SUCCESS);
  } else if(argc != 1) {
    printf("usage: cpt [-v]\n");
    printf("See \"cpt_conf.h\" for more information.\n");
    exit(EXIT_FAILURE);
#ifdef _REMOVE_THIS_LINE_BEFORE_BUILD_
  } else {
    printf("You have to edit \"cpt_conf.h\" before running cpt.\n");
    exit(EXIT_FAILURE);
#endif /* _REMOVE_THIS_LINE_BEFORE_BUILD_ */
  }
#ifdef USE_SYSLOG
  openlog("cpt", LOG_PID, LOG_DAEMON);
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "open syslog\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
#endif /* USE_SYSLOG */
#ifdef USE_LOGFILE
  if((log_fd = open(LOG_FILE, O_WRONLY | O_APPEND | O_CREAT, 0644)) == -1) {
    printf("cannot open log_file: \""LOG_FILE"\"\n");
    exit(EXIT_FAILURE);
  }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "open log_file: \""LOG_FILE"\"\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
#endif /* USE_LOGFILE */
#if ((! defined(USE_SYSLOG)) && (! defined(USE_LOGFILE)))
    printf("warning no logging defined - You will not receive any message from cpt\n");
#endif /* ((! defined(USE_SYSLOG)) && (! defined(USE_LOGFILE))) */
#ifdef RUNAS_USER
  pw = NULL;
  if(geteuid() == 0) {
    if((pw = getpwnam(RUNAS_USER)) == NULL) {
      mprint_clean_exit(EXIT_FAILURE, "user "RUNAS_USER" does not exist\n");
    }
    endpwent();
  } else {
    mprint_clean_exit(EXIT_FAILURE, "cannot access user "RUNAS_USER"\n");
  }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "got RUNAS_USER: \""RUNAS_USER"\" (uid:gid: %d:%d)\n", pw->pw_uid, pw->pw_gid);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
#endif /* RUNAS_USER */
  /* start daemon */
  if((pid = fork()) == -1) {
    mprint_clean_exit(EXIT_FAILURE, "failure on daemon fork()\n");
  } else if(pid > 0) { /* parent - not needed */
    exit(EXIT_SUCCESS);
  }
  /* child */
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "fork to run as daemon\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  umask(0027);
  if(setsid() == -1) {
    mprint_clean_exit(EXIT_FAILURE, "failure on setsid()\n");
  }
  if((chdir(DAEMON_DIR)) == -1) {
    if((mkdir(DAEMON_DIR, 0750)) == -1) {
      mprint_clean_exit(EXIT_FAILURE, "failure on mkdir("DAEMON_DIR")\n");
    }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
    write_log(CPT_DEBUG, "created working dir \""DAEMON_DIR"\"\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
#ifdef RUNAS_USER
    if(geteuid() == 0) {
      if(chown(DAEMON_DIR, pw->pw_gid, pw->pw_uid) == -1) {
        mprint_clean_exit(EXIT_FAILURE, "failure on chown("DAEMON_DIR", "RUNAS_USER")\n");
      }
    }
#endif /* RUNAS_USER */
    if((chdir(DAEMON_DIR)) == -1) {
      mprint_clean_exit(EXIT_FAILURE, "failure on chdir("DAEMON_DIR")\n");
    }
  }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "changed to working dir \""DAEMON_DIR"\"\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  close(STDIN_FILENO); close(STDOUT_FILENO); close(STDERR_FILENO);
  in_fd = open("/dev/null", O_RDONLY); /* stdin */
#ifdef LOG_FILE
  if((in_fd = open(LOG_FILE, O_WRONLY | O_CREAT | O_APPEND, 0644)) == -1) /* stdout */
#endif /* LOG_FILE */
    in_fd = open("/dev/null", O_WRONLY);
  dup(in_fd); /* stderr */
  signal(SIGCHLD, SIG_IGN);
  signal(SIGTSTP, SIG_IGN);
  signal(SIGTTOU, SIG_IGN);
  signal(SIGTTIN, SIG_IGN);
  signal(SIGHUP, signal_handler);
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  if((geteuid() != 0) && (PORT_LISTEN < 1024)) {
    mprint_clean_exit(EXIT_FAILURE, "must be root to open port %d\n", PORT_LISTEN);
  }
#ifdef BIND_THIS_IP_ONLY
  get_addrinfo(BIND_THIS_IP_ONLY, PORT_LISTEN, &result);
#else /* BIND_THIS_IP_ONLY */
  get_addrinfo(NULL, PORT_LISTEN, &result);
#endif /* BIND_THIS_IP_ONLY */
  l_fd = 0;
  for(rp = result; rp != NULL; rp = rp->ai_next) {
    if((l_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol)) == -1) {
      continue;
    }
    if(bind(l_fd, rp->ai_addr, rp->ai_addrlen) == 0) {
      sock_opt_flag = 1;
      setsockopt(l_fd, IPPROTO_TCP, TCP_NODELAY, (char *)&sock_opt_flag, sizeof(int));
      break;
    }
    close(l_fd);
  }
  if(rp == NULL) {
    mprint_clean_exit(EXIT_FAILURE, "failure on bind()\n");
  }
  freeaddrinfo(result);
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
# ifdef BIND_THIS_IP_ONLY
  write_log(CPT_DEBUG, "bind to "BIND_THIS_IP_ONLY":%d\n", PORT_LISTEN);
# else /* BIND_THIS_IP_ONLY */
  write_log(CPT_DEBUG, "bind to port %d\n", PORT_LISTEN);
# endif /* BIND_THIS_IP_ONLY */
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
#ifdef RUNAS_USER
  if(geteuid() == 0) {
    if((setgid(pw->pw_gid) == -1) || (setuid(pw->pw_uid) == -1)) {
      mprint_clean_exit(EXIT_FAILURE, "cannot drop privileges\n");
    }
  }
#endif /* RUNAS_USER */
  setuid(getuid());
#if (defined(USE_SYSLOG) || defined(USE_LOGFILE))
  if(geteuid() == 0) {
    write_log(CPT_ERR, "security leak - cpt is running with ROOT PERMISSION\n");
  }
#endif /* (defined(USE_SYSLOG) || defined(USE_LOGFILE)) */
  if((pid_fd = creat(PID_FILE, 0640)) == -1) {
    mprint_clean_exit(EXIT_FAILURE, "cannot create pid file: \""PID_FILE"\"\n");
  }
  pid_l.l_type = F_WRLCK;
  pid_l.l_whence = SEEK_SET;
  pid_l.l_start = 0;
  pid_l.l_len = MAX_PID_LEN;
  if(fcntl(pid_fd, F_SETLK, &pid_l) == -1) {
    mprint_clean_exit(EXIT_FAILURE, "cannot lock pid file: \""PID_FILE"\"\n");
  }
  daemon_pid = getpid();
  snprintf(pid_s, MAX_PID_LEN, "%d\n", daemon_pid);
  write(pid_fd, pid_s, strlen(pid_s));
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "created pid file \""PID_FILE"\"\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  if(listen(l_fd, MAX_LISTEN_NUM_CONNECT) == -1) {
    mprint_clean_exit(EXIT_FAILURE, "failure on listen()\n");
  }
  get_addrinfo(HOST_FORWARD, PORT_FORWARD, &def_dst_addr);
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO))
  write_log(CPT_INFO, "server started\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
  /* main loop */
  while(1) {
    addr_size = sizeof(struct sockaddr_storage);
    if((in_fd = accept(l_fd, (struct sockaddr*)&in_addr, &addr_size)) == -1) {
      continue;
    }
    if((pid = fork()) == -1) {
      mprint_clean_exit(EXIT_FAILURE, "failure on client fork()\n");
    } else if(pid == 0) { /* child */
      close(l_fd);
      if(getnameinfo((struct sockaddr*)&in_addr, addr_size, client_ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST) == 0) {
        do_request(in_fd, client_ip);
        close(in_fd);
        exit(EXIT_SUCCESS);
      } else {
        mprint_clean_exit(EXIT_FAILURE, "failure on getnameinfo()\n");
      }
    } else { /* parent */
      close(in_fd);
    }
  }
  close(l_fd);
  return(0);
}

void
do_forward(int src_fd, const struct addrinfo *dst_addr, const char *first_line, int size_of_first_line) {
  const struct addrinfo *rp;
  int d_fd, len, select_fd, sock_opt_flag;
  fd_set rd;
  struct timeval tv;
  char buf[(RWBUF_SIZE + 1)];

  d_fd = 0;
  for(rp = dst_addr; rp != NULL; rp = rp->ai_next) {
    if((d_fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol)) == -1) {
      continue;
    }
    if(connect(d_fd, rp->ai_addr, rp->ai_addrlen) != -1) {
      sock_opt_flag = 1;
      setsockopt(d_fd, IPPROTO_TCP, TCP_NODELAY, (char *)&sock_opt_flag, sizeof(int));
      break;
    }
    close(d_fd);
  }
  if(rp == NULL) {
    mprint_clean_exit(EXIT_FAILURE, "cannot connect\n");
  }
  if(first_line != NULL) { /* do default forward */
    write(d_fd, first_line, size_of_first_line);
  } else { /* reply for CONNECT */
    write(src_fd, PROXY_REPLY, strlen(PROXY_REPLY));
  }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "connection established\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  select_fd = (src_fd > d_fd) ? (src_fd + 1) : (d_fd + 1);
  while(1) {
    FD_ZERO(&rd);
    FD_SET(src_fd, &rd);
    FD_SET(d_fd, &rd);
    tv.tv_sec = CONNECTION_TIMEOUT;
    tv.tv_usec = 0;
    if(select(select_fd, &rd, 0, 0, &tv) == -1) {
      if(errno == EINTR) {
        continue;
      }
      mprint_clean_exit(EXIT_FAILURE, "failure on select()\n");
    } else if(FD_ISSET(src_fd, &rd)) {
      if ((len = read(src_fd, buf, RWBUF_SIZE)) <= 0) {
        break;
      }
      if ((write(d_fd, buf, len)) == -1) {
        break;
      }
    } else if(FD_ISSET(d_fd, &rd)) {
      if ((len = read(d_fd, buf, RWBUF_SIZE)) <= 0) {
        break;
      }
      if ((write(src_fd, buf, len)) == -1) {
        break;
      }
    }
  }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "connection closed\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  close(d_fd);
}

void
do_request(int req_fd, const char *client_ip) {
#define REQUEST_NO_CHECK 0
#define REQUEST_CLIENT_IP_CHECK 1
#define REQUEST_DESTINATION_CHECK 2
#define REQUEST_HEADER_CHECK 4
  static char req[(REQUEST_SIZE + 3)], dest_host[(REQUEST_SIZE + 1)], empty[(REQUEST_SIZE + 1)];
  struct addrinfo *dst_addr;
  char proto;
  int request_check_succeeded, dest_port, req_size;
#if (! defined(USE_HOST_PORT_COMBINATION)) && (! defined(ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK))
  int i;
#endif /* (! defined(USE_HOST_PORT_COMBINATION)) && (! defined(ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK)) */
#if (! defined(ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK)) || (defined USE_CHECK_CLIENT_IP)
  int j;
#endif /* (! defined(ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK)) || (defined USE_CHECK_CLIENT_IP) */
#ifdef USE_HEADER_REQUIRED
# define REQUEST_CHECK_REQUIRED REQUEST_CLIENT_IP_CHECK | REQUEST_DESTINATION_CHECK | REQUEST_HEADER_CHECK
# define NUM_HEADER_REQUIRED sizeof_array(header_required)
  static char hreq[(REQUEST_SIZE + 1)];
  int k;
  int num_header_ok;
  int header_ok[NUM_HEADER_REQUIRED];
#else /* USE_HEADER_REQUIRED */
# define CHECK_REQUIRED REQUEST_CLIENT_IP_CHECK | REQUEST_DESTINATION_CHECK
#endif /* USE_HEADER_REQUIRED */

#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "got request from %s\n", client_ip);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  req_size = strlen("CONNECT ");
  read(req_fd, req, req_size);
  if(strncmp(req, "CONNECT ", req_size) != 0) {
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
    write_log(CPT_DEBUG, "do default forward %s->"HOST_FORWARD":%d", client_ip, PORT_FORWARD);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
    do_forward(req_fd, def_dst_addr, req, req_size);
    return;
  }
  if((req_size = get_request_line(req_fd, &req[req_size], (REQUEST_SIZE - req_size))) == -1) {
    mprint_clean_exit(EXIT_FAILURE, "failure on get_request_line()\n");
  }
  req_size = req_size + strlen("CONNECT ");
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
  write_log(CPT_DEBUG, "got CONNECT request: %s\n", req);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
  dest_port = 0;
  sscanf(req, "CONNECT %[^ ] HTTP/1.%[01]%s", dest_host, &proto, empty);
  if(((proto == 48) || (proto == 49)) && (empty[0] == 0)) {
#ifdef USE_CHECK_CLIENT_IP
    request_check_succeeded = REQUEST_NO_CHECK;
    for(j = 0; j < (sizeof_array(client_required)); j++) {
      if(strncmp(client_ip, client_required[j], strlen(client_required[j])) == 0) {
        request_check_succeeded = REQUEST_CLIENT_IP_CHECK;
        break;
      }
    }
    if (request_check_succeeded == REQUEST_NO_CHECK) {
# if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_WARN) == CPT_WARN) && defined(WARN_LEVEL) && (((WARN_LEVEL) & CPT_W_CLIENT_IP) == CPT_W_CLIENT_IP))
      write_log(CPT_WARN, "check client ip requirement not fulfilled - do default forward %s->"HOST_FORWARD":%d\n", client_ip, PORT_FORWARD);
# elif ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) /* ! (CPT_WARN || CPT_W_CLIENT_IP) */
      write_log(CPT_INFO, "check client ip requirement not fulfilled - do default forward %s->"HOST_FORWARD":%d\n", client_ip, PORT_FORWARD);
# endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
      do_forward(req_fd, def_dst_addr, req, req_size);
      return;
    }
#else /* USE_CHECK_CLIENT_IP */
    request_check_succeeded = REQUEST_CLIENT_IP_CHECK;
#endif /* USE_CHECK_CLIENT_IP */
#ifdef ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK
    request_check_succeeded = request_check_succeeded | REQUEST_DESTINATION_CHECK;
    dest_port = split_host_port(dest_host);
# if (defined(USE_SYSLOG) || defined(USE_LOGFILE))
    write_log(CPT_ERR, "security leak - cpt can CONNECT to ANY target! - current request: %s -> %s:%d\n", client_ip, dest_host, dest_port);
# endif /* (defined(USE_SYSLOG) || defined(USE_LOGFILE)) */
#else /* ! ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK */
# ifdef USE_HOST_PORT_COMBINATION
    for(j = 0; j < (sizeof_array(host_port_allow)); j++) {
      if(strcmp(dest_host, host_port_allow[j]) == 0) {
        request_check_succeeded = request_check_succeeded | REQUEST_DESTINATION_CHECK;
# else /* USE_HOST_PORT_COMBINATION */
    dest_port = split_host_port(dest_host);
    /* check if requested port is allowed */
    for(i = 0; i < (sizeof_array(port_allow)); i++) {
      if(dest_port == port_allow[i]) {
        /* check if requested host is allowed */
        for(j = 0; j < (sizeof_array(host_allow)); j++) {
          if(strcmp(dest_host, host_allow[j]) == 0) {
            request_check_succeeded = request_check_succeeded | REQUEST_DESTINATION_CHECK;
# endif /* USE_HOST_PORT_COMBINATION */
#endif /* ! ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK */
#ifdef USE_HEADER_REQUIRED
            if(get_request_line(req_fd, hreq, REQUEST_SIZE) == -1) {
              mprint_clean_exit(EXIT_FAILURE, "failure on get_request_line()\n");
            }
            /* reset all header_ok */
            for(k = 0; k < NUM_HEADER_REQUIRED; k++) {
              header_ok[k] = 0;
            }
            /* test all required header */
            num_header_ok = 0;
            while(hreq[0]) {
              for(k = 0; k < NUM_HEADER_REQUIRED; k++) {
                if((strcmp(hreq, header_required[k]) == 0) && (header_ok[k] != 1)) {
                  header_ok[k] = 1;
                  num_header_ok++;
                }
              }
# if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
              write_log(CPT_DEBUG, "header received: %s (%d/%d)\n", hreq, num_header_ok, NUM_HEADER_REQUIRED);
# endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
              if(get_request_line(req_fd, hreq, REQUEST_SIZE) == -1) {
                mprint_clean_exit(EXIT_FAILURE, "failure on get_request_line()\n");
              }
            }
            if (num_header_ok == NUM_HEADER_REQUIRED) {
              request_check_succeeded = request_check_succeeded | REQUEST_HEADER_CHECK;
            }
#endif /* USE_HEADER_REQUIRED */
#ifndef ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK
# ifndef USE_HOST_PORT_COMBINATION
            break; /* can only match once */
          }
        }
# else /* ! USE_HOST_PORT_COMBINATION */
        dest_port = split_host_port(dest_host);
# endif /* ! USE_HOST_PORT_COMBINATION */
        break; /* can only match once */
      }
    }
#endif /* ALLOW_CONNECT_TO_ANY_TARGET___DONT_USE_THIS_OPTION___SECURITY_LEAK */
    if(request_check_succeeded == (REQUEST_CHECK_REQUIRED)) {
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO))
      write_log(CPT_INFO, "open proxy tunnel: %s -> %s:%d\n", client_ip, dest_host, dest_port);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
      get_addrinfo(dest_host, dest_port, &dst_addr);
      do_forward(req_fd, dst_addr, NULL, 0);
      freeaddrinfo(dst_addr);
    } else if((request_check_succeeded & REQUEST_DESTINATION_CHECK) != REQUEST_DESTINATION_CHECK) {
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_WARN) == CPT_WARN) && defined(WARN_LEVEL) && (((WARN_LEVEL) & CPT_W_TARGET) == CPT_W_TARGET))
      write_log(CPT_WARN, "destination requirement not fulfilled - do default forward %s->"HOST_FORWARD":%d / requested target: %s:%d\n", client_ip, PORT_FORWARD, dest_host, dest_port);
#elif ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) /* ! (CPT_WARN || CPT_W_TARGET) */
      write_log(CPT_INFO, "destination requirement not fulfilled - do default forward %s->"HOST_FORWARD":%d / requested target: %s:%d\n", client_ip, PORT_FORWARD, dest_host, dest_port);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
      do_forward(req_fd, def_dst_addr, req, req_size);
    } else { /* request_check_succeeded != REQUEST_HEADER_CHECK */
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_WARN) == CPT_WARN) && defined(WARN_LEVEL) && (((WARN_LEVEL) & CPT_W_HEADER) == CPT_W_HEADER))
      write_log(CPT_WARN, "header requirement not fulfilled - do default forward %s->"HOST_FORWARD":%d\n", client_ip, PORT_FORWARD);
#elif ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) /* ! (CPT_WARN || CPT_W_HEADER) */
      write_log(CPT_INFO, "header requirement not fulfilled - do default forward %s->"HOST_FORWARD":%d\n", client_ip, PORT_FORWARD);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
      do_forward(req_fd, def_dst_addr, req, req_size);
    }
  } else {
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_WARN) == CPT_WARN) && defined(WARN_LEVEL) && (((WARN_LEVEL) & CPT_W_REQUEST) == CPT_W_REQUEST))
    write_log(CPT_WARN, "request malformed - do default forward %s->"HOST_FORWARD":%d / request: %s\n", client_ip, PORT_FORWARD, req);
#elif ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) /* ! (CPT_WARN || CPT_W_REQUEST) */
    write_log(CPT_INFO, "request malformed - do default forward %s->"HOST_FORWARD":%d / request: %s\n", client_ip, PORT_FORWARD, req);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
    req[req_size++] = '\r';
    req[req_size++] = '\n';
    req[req_size] = 0;
    do_forward(req_fd, def_dst_addr, req, req_size);
  }
  return;
}

int
get_request_line(int req_fd, char *request_line, int max_request_line_size) {
  unsigned int i;
  char c;

  for(i = 0; i < max_request_line_size; i++) {
    if(read(req_fd, &c, sizeof(char)) != sizeof(char)) {
      return(-1);
    }
    request_line[i] = c;
    if(request_line[i] == '\n') {
      break;
    }
  }
  /* trim \r */
  if((i > 0) && (request_line[(i - 1)] == '\r')) {
    i--;
  }
  request_line[i] = 0;
  return(i);
}

void
mprint_clean_exit(int exit_status, const char *msg, ...) {
#if (defined(USE_SYSLOG) || defined(USE_LOGFILE))
  va_list ap;

  va_start(ap, msg);
  if(exit_status == EXIT_FAILURE) {
    vwrite_log(CPT_ERR, msg, ap);
#if (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)
  } else {
    vwrite_log(CPT_INFO, msg, ap);
#endif /* (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO) */
  }
  va_end(ap);
#endif /* (defined(USE_SYSLOG) || defined(USE_LOGFILE)) */
  if(daemon_pid == getpid()) {
    close(pid_fd);
    freeaddrinfo(def_dst_addr);
    unlink(PID_FILE);
#ifdef USE_SYSLOG
    closelog();
#endif /* USE_SYSLOG */
#ifdef USE_LOGFILE
    close(log_fd);
#endif /* USE_LOGFILE */
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO))
    write_log(CPT_INFO, "server stoped\n");
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)) */
  }
  exit(exit_status);
}

void
get_addrinfo(const char *dhost, int dport, struct addrinfo **daddr) {
#define MAX_PORT_LEN 6
  struct addrinfo hints;
  char dport_s[(MAX_PORT_LEN + 1)];
  int addr_err;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_protocol = 0;
  hints.ai_canonname = NULL;
  hints.ai_addr = NULL;
  hints.ai_next = NULL;
  snprintf(dport_s, MAX_PORT_LEN, "%d", dport);
  if((addr_err = getaddrinfo(dhost, dport_s, &hints, daddr)) != 0) {
    mprint_clean_exit(EXIT_FAILURE, "getaddrinfo: %s\n", gai_strerror(addr_err));
  }
}

void
signal_handler(int sig) {
  switch(sig) {
    case SIGHUP:
      mprint_clean_exit((128 + 1), "exit by hangup signal\n");
      break;
    case SIGTERM:
      mprint_clean_exit((128 + 9), "exit by term signal\n");
      break;
  }
}

int
split_host_port(char *host_port) {
    int i, j, dest_port;

    j = 0;
    for(i = 0; host_port[i] != 0; i++) {
      if(host_port[i] == ':') {
        j = i; /* get last ':' as port separator */
      }
    } 
    host_port[i] = 0;
    dest_port = 0;
    if(j != 0) {
      dest_port = atoi(&host_port[(j + 1)]);
      host_port[j] = 0;
    }
#if ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG))
              write_log(CPT_DEBUG, "split dest - host: %s, Port: %d\n", host_port, dest_port);
#endif /* ((defined(USE_SYSLOG) || defined(USE_LOGFILE)) && (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)) */
    return dest_port;
}

#if (defined(USE_SYSLOG) || defined(USE_LOGFILE))
void
vwrite_log(int cpt_log_level, const char *msg, va_list ap) {
#define MAX_MSG_LEN 255
   char msg_prefix[(MAX_MSG_LEN + 1)];
# ifdef USE_LOGFILE
  char file_msg[(MAX_MSG_LEN + 1)];
# endif /* USE_LOGFILE */

  if(cpt_log_level == CPT_ERR) {
    snprintf(msg_prefix, MAX_MSG_LEN, "error: %s", msg);
# ifdef USE_SYSLOG
    vsyslog(LOG_ERR, msg_prefix, ap);
# endif /* USE_SYSLOG */
# ifdef USE_LOGFILE
    vsnprintf(file_msg, MAX_MSG_LEN, msg_prefix, ap);
    write(log_fd, file_msg, strlen(file_msg));
# endif /* USE_LOGFILE */
# if (((USE_SYSLOG | USE_LOGFILE) & CPT_WARN) == CPT_WARN)
  } else if(cpt_log_level == CPT_WARN) {
    snprintf(msg_prefix, MAX_MSG_LEN, "warning: %s", msg);
#  if defined(USE_SYSLOG) && (((USE_SYSLOG) & CPT_WARN) == CPT_WARN)
    vsyslog(LOG_WARNING, msg_prefix, ap);
#  endif /* defined(USE_SYSLOG) && (((USE_SYSLOG) & CPT_WARN) == CPT_WARN) */
#  if defined(USE_LOGFILE) && (((USE_LOGFILE) & CPT_WARN) == CPT_WARN)
    vsnprintf(file_msg, MAX_MSG_LEN, msg_prefix, ap);
    write(log_fd,file_msg, strlen(file_msg));
#  endif /* defined(USE_LOGFILE) && (((USE_LOGFILE) & CPT_WARN) == CPT_WARN) */
# endif /* (((USE_SYSLOG | USE_LOGFILE) & CPT_WARN) == CPT_WARN) */
# if (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO)
  } else if(cpt_log_level == CPT_INFO) {
    snprintf(msg_prefix, MAX_MSG_LEN, "info: %s", msg);
#  if defined(USE_SYSLOG) && (((USE_SYSLOG) & CPT_INFO) == CPT_INFO)
    vsyslog(LOG_INFO, msg_prefix, ap);
#  endif /* defined(USE_SYSLOG) && (((USE_SYSLOG) & CPT_INFO) == CPT_INFO) */
#  if defined(USE_LOGFILE) && (((USE_LOGFILE) & CPT_INFO) == CPT_INFO)
    vsnprintf(file_msg, MAX_MSG_LEN, msg_prefix, ap);
    write(log_fd,file_msg, strlen(file_msg));
#  endif /* defined(USE_LOGFILE) && (((USE_LOGFILE) & CPT_INFO) == CPT_INFO) */
# endif /* (((USE_SYSLOG | USE_LOGFILE) & CPT_INFO) == CPT_INFO) */
# if (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)
  } else if(cpt_log_level == CPT_DEBUG) {
    snprintf(msg_prefix, MAX_MSG_LEN, "debug: %s", msg);
#  if defined(USE_SYSLOG) && (((USE_SYSLOG) & CPT_DEBUG) == CPT_DEBUG)
    vsyslog(LOG_DEBUG, msg_prefix, ap);
#  endif /* defined(USE_SYSLOG) && (((USE_SYSLOG) & CPT_DEBUG) == CPT_DEBUG) */
#  if defined(USE_LOGFILE) && (((USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG)
    vsnprintf(file_msg, MAX_MSG_LEN, msg_prefix, ap);
    write(log_fd, file_msg, strlen(file_msg));
#  endif /* defined(USE_LOGFILE) && (((USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG) */
# endif /* (((USE_SYSLOG | USE_LOGFILE) & CPT_DEBUG) == CPT_DEBUG) */
  } else {
    /* should never reach this */
    snprintf(msg_prefix, MAX_MSG_LEN, "error: log level(%d) not defined / original message: %s", cpt_log_level, msg);
# ifdef USE_SYSLOG
    vsyslog(LOG_ERR, msg_prefix, ap);
# endif /* USE_SYSLOG */
# ifdef USE_LOGFILE
    vsnprintf(file_msg, MAX_MSG_LEN, msg_prefix, ap);
    write(log_fd, file_msg, strlen(file_msg));
# endif /* USE_LOGFILE */
  }
}
void
write_log(int cpt_log_level, const char *msg, ...) {
  va_list ap;
  va_start(ap, msg);
  vwrite_log(cpt_log_level, msg, ap);
  va_end(ap);
}

#endif /* (defined(USE_SYSLOG) || defined(USE_LOGFILE)) */

