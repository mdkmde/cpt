# (c) 2008,2009 maro (@macarony.de - Matthias Diener)
VERSION = 1.4

# edit PATH to customize installation
PREFIX = /usr/local

CC = cc
CFLAGS = -O2 -pedantic -Wall -DVERSION=\"${VERSION}\"
DIST_FILES = cpt.c cpt_conf.def.h stunnel_cpt.conf Makefile README LICENSE.html draft-luotonen-web-proxy-tunneling-01.txt

all: cpt

cpt: cpt.c cpt_conf.h
	@${CC} ${CFLAGS} -o cpt cpt.c
	@chmod 755 $@
	@strip $@

cpt_conf.h:
	@echo -e "No \"cpt_conf.h\" found .. copy \"cpt_conf.def.h\"\n - edit cpt_conf.h before build\n"
	@cp cpt_conf.def.h $@

clean:
	@rm -f cpt cpt-${VERSION}.tar.gz

dist: clean
	@mkdir -p cpt-${VERSION}
	@cp -R ${DIST_FILES} cpt-${VERSION}
	@if [ $${UID} -eq 0 ]; then \
	  chown -R 1000:1000 cpt-${VERSION}; \
	fi
	@chmod 755 cpt-${VERSION};
	@chmod -R 644 cpt-${VERSION}/*;
	@tar -cf cpt-${VERSION}.tar cpt-${VERSION}
	@gzip cpt-${VERSION}.tar
	@if [ $${UID} -eq 0 ]; then \
	  chown -R 1000:1000 cpt-${VERSION}.tar.gz; \
	fi
	@rm -rf cpt-${VERSION}

install: all
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f cpt ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/cpt

uninstall:
	@rm -f ${DESTDIR}${PREFIX}/bin/cpt

.PHONY: all clean dist install uninstall

